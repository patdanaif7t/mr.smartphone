package com.example.applicationmrsmartphone

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.room.Room
import com.example.applicationmrsmartphone.database.AppDatabase
import com.example.applicationmrsmartphone.database.Mobile
import com.example.applicationmrsmartphone.databinding.ActivityMainBinding
import com.example.applicationmrsmartphone.ui.home.Home
import com.example.applicationmrsmartphone.ui.home.Phone

import timber.log.Timber


class MainActivity() : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding
    private lateinit var drawerLayout: DrawerLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.plant(Timber.DebugTree())
        Timber.i("onCreate called")
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        drawerLayout = binding.drawerLayout


//        val db = Room.databaseBuilder(
//            applicationContext,
//            AppDatabase::class.java,"mobileDB"
//        ).build()


//        Thread {
//            val mob = Mobile()
//            mob.mbId = 1234568
//            mob.mbName = "IPhone 1222"
//            mob.mbDes = "IPhone 1222 Dess"
//            db.mobDao().saveMobile(mob)
//            db.mobDao().deleteMobile(mob)
//            db.mobDao().getMobile().forEach {
//                Timber.i("Log Start")
//                Timber.i("Mobile Name ${it.mbName}")
//                Timber.i("Mobile ID ${it.mbId}")
//                Timber.i("Mobile Des  ${it.mbDes}")
//            }
//        }.start()


        val navController = this.findNavController(R.id.myNavHostFragment)

        NavigationUI.setupActionBarWithNavController(this,navController, drawerLayout)

        NavigationUI.setupWithNavController(binding.navView, navController)



    }



    override fun onSupportNavigateUp(): Boolean {
        val navController = this.findNavController(R.id.myNavHostFragment)
        return NavigationUI.navigateUp(navController, drawerLayout)
    }


}
