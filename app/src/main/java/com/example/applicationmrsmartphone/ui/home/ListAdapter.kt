package com.example.applicationmrsmartphone.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.applicationmrsmartphone.R
import com.example.applicationmrsmartphone.database.Mobile


class ListAdapter(private val list: List<Mobile>)
    : RecyclerView.Adapter<PhoneViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return PhoneViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: PhoneViewHolder, position: Int) {
        val phone: Mobile = list[position]
        holder.bind(phone)
    }

    override fun getItemCount(): Int = list.size

}



class PhoneViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
    private var mTitleView: TextView? = null
    private var mDesView: TextView? = null

    init {
        mTitleView = itemView.findViewById(R.id.list_title)
        mDesView = itemView.findViewById(R.id.list_description)
    }

    fun bind(phone: Mobile) {
        mTitleView?.text = phone.mbName
        mDesView?.text = phone.mbDes.toString()

    }

}