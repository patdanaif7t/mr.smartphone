package com.example.applicationmrsmartphone.ui.iphonexr

import android.util.Log
import androidx.lifecycle.ViewModel

class iphonexrViewModel : ViewModel() {
    init {
        Log.i("iphonexrViewModel","IPhoneXRViewModel created!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("iphonexrViewModel","IPhoneXRViewModel destroyed!")
    }
}