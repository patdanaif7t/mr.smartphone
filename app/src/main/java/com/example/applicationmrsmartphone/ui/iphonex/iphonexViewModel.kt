package com.example.applicationmrsmartphone.ui.iphonex
import android.util.Log
import androidx.lifecycle.ViewModel

class iphonexViewModel : ViewModel () {
    init {
        Log.i("iphonexViewModel","IPhoneXViewModel created!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("iphonexViewModel","IPhoneXViewModel destroyed!")
    }
}