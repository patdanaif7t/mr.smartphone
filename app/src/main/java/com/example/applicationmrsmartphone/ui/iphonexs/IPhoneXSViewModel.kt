package com.example.applicationmrsmartphone.ui.iphonexs

import android.util.Log
import androidx.lifecycle.ViewModel

class IPhoneXSViewModel : ViewModel () {
    init {
        Log.i("IPhoneXSViewModel","IPhoneXSViewModel created!")
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("IPhoneXSViewModel","IPhoneXSViewModel destroyed!")
    }
}