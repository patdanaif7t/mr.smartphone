package com.example.applicationmrsmartphone.ui.home


import android.app.Application
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.applicationmrsmartphone.R
import com.example.applicationmrsmartphone.database.AppDatabase
import com.example.applicationmrsmartphone.database.Mobile
import com.example.applicationmrsmartphone.database.MobileDao
import com.example.applicationmrsmartphone.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.*
import timber.log.Timber

data class Phone(val title: String, val description: String)

class Home : Fragment() {

    private lateinit var addDataJob: Job
    private lateinit var database: MobileDao
    private lateinit var application: Application
    private lateinit var uiScope: CoroutineScope
    var mob = listOf<Mobile>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(inflater,
            R.layout.fragment_home,container,false)

        addDataJob = Job()
        application = requireNotNull(this.activity).application
        uiScope = CoroutineScope(Dispatchers.Main + addDataJob)
        database = AppDatabase.getInstance(application).mobDao()
        uiScope.launch {
            getAllData()
            topphone.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = ListAdapter(mob)
            }
        }

        binding.btnPassdata.setOnClickListener{
            val action = HomeDirections.actionHomeToNavIPhoneXR()
            action.setPassData("IPhone Xr")
            Navigation.findNavController(it).navigate(action)
        }

        binding.btnAdd.setOnClickListener {
            Navigation.findNavController(it).navigate((HomeDirections.actionHomeToAddPhone()))
        }



        retainInstance = true
        setHasOptionsMenu(true)

        return binding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.options_menu,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,view!!.findNavController())
                ||super.onOptionsItemSelected(item)
    }



    private suspend fun getAllData() {
        withContext(Dispatchers.IO) {
            Timber.i("Get Mobile")
            mob = database.getMobile()
        }
    }






}
