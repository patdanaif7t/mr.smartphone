package com.example.applicationmrsmartphone.ui.addphone


import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import com.example.applicationmrsmartphone.R
import com.example.applicationmrsmartphone.database.AppDatabase
import com.example.applicationmrsmartphone.database.Mobile
import com.example.applicationmrsmartphone.database.MobileDao
import com.example.applicationmrsmartphone.databinding.FragmentAddPhoneBinding
import kotlinx.coroutines.*
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 */
class addPhone : Fragment() {

    private lateinit var addDataJob: Job
    private lateinit var database: MobileDao
    private lateinit var application: Application
    private lateinit var uiScope: CoroutineScope

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentAddPhoneBinding>(inflater,
            R.layout.fragment_add_phone,container,false)
        addDataJob = Job()
        application = requireNotNull(this.activity).application
        uiScope = CoroutineScope(Dispatchers.Main + addDataJob)
        database = AppDatabase.getInstance(application).mobDao()

        binding.btnDelete.setOnClickListener{
            uiScope.launch {
                val mob = Mobile()
                mob.mbId = binding.editId.text.toString().toInt()
                mob.mbName = binding.editName.text.toString()
                mob.mbDes = binding.editDes.text.toString()
                binding.editId.setText("")
                binding.editName.setText("")
                binding.editDes.setText("")
                deleteMobile(mob)
            }
        }

        binding.btnAddphone.setOnClickListener{
            uiScope.launch {
                val mob = Mobile()
                mob.mbId = binding.editId.text.toString().toInt()
                mob.mbName = binding.editName.text.toString()
                mob.mbDes = binding.editDes.text.toString()
                binding.editId.setText("")
                binding.editName.setText("")
                binding.editDes.setText("")
                insert(mob)
            }
//            Navigation.findNavController(it).navigate((addPhoneDirections.actionAddPhoneToHome()))
        }

        binding.btnUpdate.setOnClickListener{
            uiScope.launch {
                val mob = Mobile()
                mob.mbId = binding.editId.text.toString().toInt()
                mob.mbName = binding.editName.text.toString()
                mob.mbDes = binding.editDes.text.toString()
                binding.editId.setText("")
                binding.editName.setText("")
                binding.editDes.setText("")
                update(mob)
            }
        }

        binding.btnGetDB.setOnClickListener{
            uiScope.launch {
                getAll()
            }
        }
        // Inflate the layout for this fragment
        return binding.root
    }

    private suspend fun update(mobile: Mobile) {
        withContext(Dispatchers.IO) {
            Timber.i("Update Mobile")
            database.updateMobile(mobile)
        }
    }

    private suspend fun insert(mobile: Mobile) {
        withContext(Dispatchers.IO) {
            database.saveMobile(mobile)
            Timber.i("Saving")
            database.getMobile().forEach {
                Timber.i("Log Start")
                Timber.i("Mobile ID ${it.mbId}")
                Timber.i("Mobile Name ${it.mbName}")
                Timber.i("Mobile Des  ${it.mbDes}")
            }
        }
    }

    private suspend fun deleteMobile(mobile: Mobile) {
        withContext(Dispatchers.IO) {
            database.deleteMobile(mobile)
            Timber.i("Delete")
            database.getMobile().forEach {
                Timber.i("Log Start")
                Timber.i("Mobile ID ${it.mbId}")
                Timber.i("Mobile Name ${it.mbName}")
                Timber.i("Mobile Des  ${it.mbDes}")
            }
        }
    }

    private suspend fun getAll() {
        withContext(Dispatchers.IO) {
            database.getMobile().forEach {
                Timber.i("Get DB")
                Timber.i("Mobile ID ${it.mbId}")
                Timber.i("Mobile Name ${it.mbName}")
                Timber.i("Mobile Des  ${it.mbDes}")
            }
        }
    }



}
