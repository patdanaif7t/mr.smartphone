package com.example.applicationmrsmartphone.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Mobile {

    @PrimaryKey
    var mbId : Int? = null

    @ColumnInfo(name = "mob_name")
    var mbName : String? = null

    @ColumnInfo(name = "mob_des")
    var mbDes : String? = null
}