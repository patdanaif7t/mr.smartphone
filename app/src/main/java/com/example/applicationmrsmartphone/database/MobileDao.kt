package com.example.applicationmrsmartphone.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
@Dao
interface MobileDao {
    @Insert
    fun saveMobile(mob : Mobile)

    @Update()
    fun updateMobile(mob : Mobile)

    @Query("select * from mobile")
    fun  getMobile() : List<Mobile>

    @Delete
    fun deleteMobile(mob: Mobile)


    @Query("DELETE  FROM mobile")
    fun clear()
}